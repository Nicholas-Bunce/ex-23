﻿using System;

namespace ex_23
{
using System.Collections.Generic;

    class Program
    
    {
        static void Main(string[] args)
        
        {
           var movies = new Dictionary<string, string> ();

            movies.Add("wanted", "Action");
            movies.Add("cars", "animation");
            movies.Add("mouna", "kids");
            movies.Add("sleeping", "romance");
            movies.Add("Red", "Action");

            foreach (var x in movies)
            {
                Console.WriteLine($"{x.Key} is a {x.Value}");
            }
        }
    }
}

